--DROP SCHEMA IF EXISTS alpha;
--CREATE SCHEMA alpha;
--USE alpha;
-- SET AUTOCOMMIT=0;


--create user 'alphasql'@'localhost' identified by 'alphasql01';
--grant all privileges on alpha.* to 'alphasql'@'localhost';

-- Para que tenga permisos desde fuera de docker

create user 'alphasql'@'%' identified by 'alphasql01';
grant all privileges on alpha.* to 'alphasql'@'%';
alter user 'alphasql'@'%' IDENTIFIED WITH mysql_native_password BY 'alphasql01';

--
-- Tabla CLIENTES
--

DROP TABLE IF EXISTS clientes;
CREATE TABLE IF NOT EXISTS CLIENTES (
    id_cliente INT AUTO_INCREMENT,
    genero TINYINT(1) NOT NULL,
    nombre VARCHAR(32) NOT NULL,
    apellidos VARCHAR(32) NOT NULL,
    email VARCHAR(64) NOT NULL,
    passwd VARCHAR(32) NOT NULL,
    last_passwd_gen TIMESTAMP NOT NULL,
    activo TINYINT(1) NOT NULL,
    PRIMARY KEY (id_cliente)
);

--
-- Tabla EMPLEADOS, PERFIL EMPLEADOS, PRIVILEGIOS EMPLEADOS
--

DROP TABLE IF EXISTS empleados;
CREATE TABLE IF NOT EXISTS empleados (
    id_empleado INT AUTO_INCREMENT,
    id_perfil INT NOT NULL,
    nombre VARCHAR(32) NOT NULL,
    apellidos VARCHAR(32) NOT NULL,
    email VARCHAR(64) NOT NULL,
    passwd VARCHAR(32) NOT NULL,
    last_passwd_gen TIMESTAMP NOT NULL,
    activo TINYINT(1) NOT NULL,
    PRIMARY KEY (id_empleado)
);

DROP TABLE IF EXISTS perfil_empleados;
CREATE TABLE IF NOT EXISTS perfil_empleados (
    id_perfil INT AUTO_INCREMENT,
    nombre_perfil VARCHAR(32) NOT NULL,
    PRIMARY KEY (id_empleado)
);

DROP TABLE IF EXISTS privilegios;
CREATE TABLE IF NOT EXISTS privilegios (
    id_priv INT AUTO_INCREMENT,
    id_perfil INT AUTO_INCREMENT,
    view TINYINT(1) NOT NULL,
    add TINYINT(1) NOT NULL,
    edit TINYINT(1) NOT NULL,
    delete TINYINT(1) NOT NULL,
    PRIMARY KEY (id_priv)
);



insert into CLIENTES value (1,1,'Alejandro','Jimenez','alejandro.jbr@gmail.com','bootcamp4',now(),1);
insert into CLIENTES (genero,nombre,apellidos,email,passwd,last_passwd_gen,activo) VALUES (1,'Manolito','Mr PHP','manolito@mmm.ci',md5('11111111'),now(),1);

-- Picapiedra : 1111

select * from CLIENTES where passwd=md5(1111);